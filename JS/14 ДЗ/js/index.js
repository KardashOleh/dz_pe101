"use strict";

const btnStop = document.querySelector('.stop');
const btnStart = document.querySelector('.start');
let slides = document.querySelectorAll('.image-to-show');

// =============================================================================

const themeButton = document.querySelector('.theme');
const allButtons = document.querySelectorAll('.btn');

const local = localStorage.getItem('them');
// console.log(local);
if (local === 'dark') {
    for (const btn of allButtons) {
        btn.classList.add('cssBtn');
    };
    document.body.classList.add('dark');
};

themeButton.addEventListener('click', () => {
    for (const btn of allButtons) {
        btn.classList.toggle('cssBtn');
    };
    document.body.classList.toggle('dark');
    const local2 = localStorage.getItem('them');
    if (local2 === 'dark') {
        localStorage.removeItem('them')
    } else {
        localStorage.setItem('them', 'dark')
    };
});

// =============================================================================

let slider = [];
for (let i=0; i < slides.length; i++) {
    slider[i] = slides[i].src;
    slides[i].remove();
};

let startStop = '';
let step = 0;
let offset = 0;

function draw () {
    let img = document.createElement('img');
    img.src = slider[step];
    img.classList.add('image-to-show');
    img.style.left = offset*400 + 'px';
    document.querySelector('.images-wrapper').appendChild(img);
    if (step + 1 == slider.length) {
        step = 0;
    } else {
        step++;
    };
    offset = 1;
};

function left () {
    let slides2 = document.querySelectorAll('.image-to-show');
    slides2[0].remove();
    draw();
};
draw();

document.addEventListener('DOMContentLoaded', event => {
    startStop = setInterval (function buttonRoom () {
            left ();
    }, 3000);
});
btnStart.addEventListener('click', () => {
    clearInterval(startStop)
    startStop = setInterval (function buttonRoom () {
        left ();
    }, 3000);
});
btnStop.addEventListener('click', () => {
    clearInterval(startStop);
});



