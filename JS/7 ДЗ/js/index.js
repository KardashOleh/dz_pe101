"use strict";

function filterBy(arr, str) {
    const newArr = arr.filter( el => typeof el !== str);
    return newArr;
};

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));

