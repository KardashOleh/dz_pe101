"use strict";

const item = document.body;
const list = [ "Kiev", ["Borispol", "Irpin"],"Kharkiv", "Odessa", "Lviv", "Dnieper", "Vinnytsia", ["Kozyatyn", "Zhmerinka", "Mohyliv-Podilskyi", "Haysyn"], "Ternopil", "Uzhhorod"];

function listItem(list, item) {
    const ul = document.createElement("ul");
    list.map( el => {   
        const li = document.createElement("li");
        if (Array.isArray(el)) {
            const ulArr = document.createElement("ul");
            el.map( elem => {
                const liArr = document.createElement("li");
                liArr.innerText = elem;
                ulArr.append(liArr);
            });
            ulArr.style.paddingLeft = '20px';
            li.append(ulArr);
            li.style.listStyleType = 'none';
        } else {
            li.innerText = el;
        };
        ul.append(li);
    });
    item.append(ul);
};

listItem(list, item);



    // list.map( el => {   
    //     const li = document.createElement("li");
    //     if (Array.isArray(el)) {
    //         console.log(el);
    //         const ulArr = document.createElement("ul");
    //         el.map( elem => {
    //             console.log(elem);
    //             const liArr = document.createElement("li");
    //             liArr.innerText = elem;
    //             ulArr.append(liArr);
    //         });
    //         ulArr.style.paddingLeft = '20px';
    //         li.append(ulArr);
    //         li.style.listStyleType = 'none';
    //     } else {
    //         li.innerText = el;
    //     };
    //     ul.append(li);
    // });