"use strict";

let form = document.querySelector('.password-form');
let textError = document.createElement('p');
textError.style.height = '18px';
textError.style.marginTop = '0px';
textError.style.color = 'red';
document.querySelector('.btn').before(textError);

form.addEventListener('click', (event) => {
    if (event.target.tagName === 'I') {
        event.target.classList.toggle('fa-eye');
        event.target.classList.toggle('fa-eye-slash');

        let label = event.target.closest('.input-wrapper');
        let input = label.querySelector('input');
        input.type = input.getAttribute('type') === 'password' ? 'text' : 'password';
    };
});

form.addEventListener('submit', (event) => {
    event.preventDefault();
    let inputs = form.querySelectorAll('input');

    if (form.querySelector('p').textContent) {
        form.querySelector('p').remove();
    };
    
    if (inputs[0].value === inputs[1].value) {
        alert('You are welcome');
        inputs[0].value = '';
        inputs[1].value = '';
    } else {
        textError.textContent = 'Потрібно ввести одинакові паролі';
        document.querySelector('.btn').before(textError);
    };

    inputs.forEach((input) => {
        input.addEventListener('focus', () => {
            textError.textContent = '';
        });
    });
});