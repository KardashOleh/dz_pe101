"use strict";

// 1 спосіб


// document.addEventListener('keydown', (event) => {
//     const buttons = document.querySelectorAll('.btn');
//     console.log(event);
//     for(let btn of buttons) {
//         if (btn.classList.contains("dark")) btn.classList.remove("dark");
//         if (event.key === 'Enter' || event.key === 'NumpadEnter') buttons[0].classList.add("dark");
//         if (event.key === 'KeyS') buttons[1].classList.add("dark");
//         if (event.key === 'KeyE') buttons[2].classList.add("dark");
//         if (event.key === 'KeyO') buttons[3].classList.add("dark");
//         if (event.key === 'KeyN') buttons[4].classList.add("dark");
//         if (event.key === 'KeyL') buttons[5].classList.add("dark");
//         if (event.key === 'KeyZ') buttons[6].classList.add("dark");
//     };
    
// });

// 2 спосіб


const buttons = document.querySelectorAll('.btn');

for (let btn of buttons) {
    // console.log(btn, btn.textContent);
    // console.log(btn.textContent.toLowerCase());

    if (btn.innerHTML === "Enter") {
        btn.classList.add('Enter');
        console.log(btn);
    } else {
        // btn.classList.add(`Key${btn.innerHTML}`);
        btn.classList.add(btn.innerHTML.toLowerCase());
        console.log(btn);
    };
};

document.addEventListener("keydown", event => {
    // let keyCode = event.code;
    let keyCode = event.key;
    console.log(keyCode);
    if(document.querySelector(`.${keyCode}`)){
        for (const btn of buttons) {
            // btn.style.backgroundColor = '';
            btn.classList.remove("dark");
        }
        // document.querySelector(`.${keyCode}`).style.backgroundColor = '#0000FF';
        document.querySelector(`.${keyCode}`).classList.add("dark");
    };
});
