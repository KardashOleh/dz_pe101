"use strict";

function createNewUser() {
    let str, str2;
    str = prompt("Введите свое имя");
    str2 = prompt("Введите свою фамилию");

    const newUser = {
        lastName: str,
        firstName: str2,
        
        getLogin () {
            // console.log(this.lastName);
            return this.lastName[0].toLowerCase()+this.firstName.toLowerCase();
        },
        setLastName(value) {
            return this.lastName = value;
        },
        setFirstName(value) {
            return this.firstName = value;
        },

    };

    return newUser;
    
};

const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.setLastName('Dima'));
console.log(newUser.setFirstName('Petrenko'));
console.log(newUser);