"use strict";

const btnStop = document.querySelector('.stop');
const btnStart = document.querySelector('.start');
let slides = document.querySelectorAll('.image-to-show');

let slider = [];
for (let i=0; i < slides.length; i++) {
    slider[i] = slides[i].src;
    slides[i].remove();
};

let startStop = '';
let step = 0;
let offset = 0;

function draw () {
    let img = document.createElement('img');
    img.src = slider[step];
    img.classList.add('image-to-show');
    img.style.left = offset*400 + 'px';
    document.querySelector('.images-wrapper').appendChild(img);
    if (step + 1 == slider.length) {
        step = 0;
    } else {
        step++;
    };
    offset = 1;
};

function left () {
    let slides2 = document.querySelectorAll('.image-to-show');
    slides2[0].remove();
    draw();
};
draw();

document.addEventListener('DOMContentLoaded', event => {
    startStop = setInterval (function buttonRoom () {
            left ();
    }, 3000);
});
btnStart.addEventListener('click', () => {
    clearInterval(startStop)
    startStop = setInterval (function buttonRoom () {
        left ();
    }, 3000);
});
btnStop.addEventListener('click', () => {
    clearInterval(startStop);
});






// const btnStop = document.querySelector('.stop');
// const btnStart = document.querySelector('.start');
// const imageCollection = document.querySelectorAll('.image-to-show');
// let counterImg = 1;
// let startStop = '';

// function showImg() {
//     for (const img of imageCollection) {
//         img.classList.add('hidden');
//     }
//     imageCollection[counterImg].classList.remove('hidden');
//     counterImg++;
//     if (counterImg >= imageCollection.length) {
//         counterImg = 0;
//     }
// }
// document.addEventListener('DOMContentLoaded', event => {
//     startStop = setInterval(showImg, 3000);
// })
// btnStart.addEventListener('click', event => {
//     clearInterval(startStop)
//     startStop = setInterval(showImg, 3000);
// })
// btnStop.addEventListener('click', event => {
//     clearInterval(startStop);
// })  
