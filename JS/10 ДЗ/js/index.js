"use strict";

const buttons = document.querySelectorAll('.tabs-title');
buttons.forEach((btn) => {
    btn.addEventListener('click', clickButton);
});

function clickButton(event) {
    let buttonAtr = this.getAttribute('data-text');
    let text = document.querySelector(`.tabs-content [data-text = ${buttonAtr}]`);
    document.querySelector('.blok').classList.remove('blok');
    text.classList.add('blok');
    document.querySelector('.tabs-title.active').classList.remove('active');
    this.classList.add('active');
};