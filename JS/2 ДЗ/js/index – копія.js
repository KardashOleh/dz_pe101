"use strict";

let name = prompt("Введите имя.");
let age = prompt("Введите ваш возраст.");

while (age === "" || age === null || isNaN(age) || !isNaN(name)) {
    
    if (!isNaN(name)) {
        name = prompt("Введите имя.");
    };
    if (age === "" || age === null || isNaN(age)) {
        age = prompt("Введите ваш возраст.");
    };
};
if (age < 18) {
    alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <=22) {

    if (confirm("Are you sure you want to continue?")) {
        alert(`«✅ Welcome, ${name}»`);
    } else {
        alert("You are not allowed to visit this website.");
    };   
} else {
    alert(`«✅ Welcome, ${name}»`);
};
