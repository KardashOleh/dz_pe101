import {useField, Field, ErrorMessage} from "formik";
import './Textarea.scss'

const Textarea = (props) => {
	const [field, meta] = useField(props);
	const {type, placeholder, className, label, name, rows} = props;
	return (
		<>
			<label className={className}>
				<p className="">{label}</p>
				<Field as="textarea" type={type} className="" name={name} rows={rows}
					   placeholder={placeholder} {...field} />
				{!!meta.error && meta.touched && <ErrorMessage name={name} className='error-textarea' component={"p"}/>}
			</label>
		</>
	)

}

export default Textarea;