import {ErrorMessage, Field, useField} from "formik";
import './Input.scss'

const Input = (props) => {
	const [field, meta] = useField(props);
	const {type, placeholder, className, label, name} = props;
	return (
		<>
			<label className={className}>
				<p className="">{label}</p>
				<Field type={type} className="" name={name} placeholder={placeholder} {...field} />
				{!!meta.error && meta.touched && <ErrorMessage name={name} className='error-input' component={"p"}/>}
			</label>
		</>
	)

}

export default Input;