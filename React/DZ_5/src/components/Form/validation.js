import * as yup from "yup"

export const validationSchema = yup.object({
	name: yup
		.string()
		.min(2, "Занадто коротке ім'я")
		.matches(/[a-zA-Z]/, "Поле містить тільки літери")
		.required("Поле обов'язкове для заповнення"),
	lastName: yup
		.string()
		.min(2, "Занадто коротке ім'я")
		.matches(/[a-zA-Z]/, "Поле містить тільки літери")
		.required("Поле обов'язкове для заповнення"),
	age: yup
		.string()
		.min(1, "Не вірний вік")
		.max(3, "Не вірний вік")
		.matches(/[0-9]/, "Поле містить тільки цифри ")
		.required("Поле обов'язкове для заповнення"),
	address: yup
		.string()
		.min(10, "Занадто коротка адреса")
		.required("Поле обов'язкове для заповнення"),
	phone: yup
		.string()
		.min(11, "Введено не повний номер")
		.max(11, "Занадто довгий номер")
		.matches(/[0-9]/, "Поле містить тільки цифри ")
		.required("Поле обов'язкове для заповнення"),

})