import {Formik, Form} from "formik";
import {validationSchema} from "./validation"
import Input from "./Input/Input";
import Textarea from "./Textarea/Textarea";
import {actionForm} from "../../store/slices/app.slice";
import {useDispatch, useSelector} from "react-redux";
import {actionSetItems} from '../../store/slices/cart.slice'


const FormCart = ({totalAmount, handleTitleSubmit}) => {
    const dispatch = useDispatch();
	const itemsBuy = useSelector(store => store.carts.items)

	const handleSubmitForm =  (values, {resetForm}) => {
		console.log(`user info  ${JSON.stringify(values)}`);
		console.log(`buy items  ${JSON.stringify(itemsBuy)}`);
		console.log(`total amount is: ${totalAmount}`)
		resetForm();
		 handleTitleSubmit();
		 dispatch(actionForm(false));
		dispatch(actionSetItems([]))
	}

	const initialValues = {
		name: '',
		lastName: '',
		age: '',
		address: '',
		phone: ''
	}

	return (
		<Formik initialValues={initialValues}
				onSubmit={handleSubmitForm}
				validationSchema={validationSchema}>

			{({isValid,}) => (
				<Form>
					<h3>Оформити замовлення</h3>
					<Input name='name' type='text' placeholder="Введіть Ваше ім'я" label="Ваше ім'я"/>
					<Input name='lastName' type='text' placeholder="Введіть Ваше прізвище" label="Ваше прізвище" />
					<Input name='age' type='text' placeholder="Введіть Ваш вік" label="Ваш вік"/>
					<Textarea name='address' type='text' placeholder="Введіть Вашу адресу" label="Ваша адреса"/>
					<Input name='phone' type='text' placeholder="Введіть Ваш телефон" label="Ваш номер телефону"/>

					<button type='submit' disabled={!isValid}>Підтвердити замовлення</button>
				</Form>
			)}
		</Formik>
	)
}
export default FormCart;

