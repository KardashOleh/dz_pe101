import {configureStore} from "@reduxjs/toolkit";
import cardReducer from "./slices/card.slice";
import favoriteReducer from './slices/favorite.slice'
import cartReducer from "./slices/cart.slice";
import appReducer from "./slices/app.slice";

const store = configureStore({
	reducer: {
		cards: cardReducer,
		favorite: favoriteReducer,
		carts: cartReducer,
		app: appReducer,
	},
})


export default store;