import {createSlice} from "@reduxjs/toolkit";

const cardSlice = createSlice({
	name: 'cards',
	initialState: {
		data: [],
	},

	reducers: {
		actionAddCards: (state, action) => {
			state.data = action.payload;
		}
	}
})

export const {actionAddCards} = cardSlice.actions

export const actionFetchCards = () => async (dispatch) => {
	const items = await fetch('./data.json').then(res => res.json());
	dispatch(actionAddCards(items));
}

export default cardSlice.reducer

