import {createSlice} from "@reduxjs/toolkit";

const favoriteSlice = createSlice({
	name: 'favorite',
	initialState: {
		data: [],
	},

	reducers: {
		actionSetFavorite: (state, {payload})=>{
			state.data = payload;
		},

		actionAddToFavorite: ( state, {payload}) => {
		 	const index = state.data.findIndex((el) => el.id ===  payload.id)

				if (index === -1) {
					state.data.push(payload)
				}
				else {
					state.data.splice(index, 1)
				}
				localStorage.setItem(`favoriteItems`, JSON.stringify(state.data));
			}
	}
})


export const {actionAddToFavorite, actionSetFavorite} = favoriteSlice.actions

export default favoriteSlice.reducer