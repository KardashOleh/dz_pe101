import './CartPage.scss'
import Button from "../../components/Button/Button";
import Modal from "../../components/Modal/Modal";
import CardFromCart from "../../components/CardFromCart/CardFromCart";
import {useDispatch, useSelector} from "react-redux";
import {actionModal} from "../../store/slices/app.slice";
import {actionDeleteCartItem} from "../../store/slices/cart.slice";


const CheckoutPage = () => {
	const dispatch = useDispatch();
	const carts = useSelector(store => store.carts.items);
	const isOpenModal = useSelector(store => store.app.isOpenModal);
	const currentCard = useSelector(store => store.app.currentData);


	const list = carts?.map(({url, name, count, price, id}) => (
		<li key={id}>
			<CardFromCart
				url={url}
				name={name}
				count={count}
				id={id}
				price={price}
			/>
		</li>
	))

	const totalAmount = carts.map(({price, count}) => price * count).reduce((prev, curr) => prev + curr, 0)

	return (
		<>
			<h1>Оформлення замовлення</h1>
			<ul className='list-cart'>
				{list}
				<span className='total-price'>Всього: {totalAmount} грн</span>
			</ul>
			{isOpenModal && (
				<Modal isCloseButton
					   headerModal={'Ви дійсно хочете видалити книгу з кошика:'}
					   action={
						   <div className='wrapper-btn'>
							   <Button type={"button"} className='btn-modal'
									   handleClick={() => {
										   {
											   dispatch(actionDeleteCartItem(currentCard))
											   dispatch(actionModal(false));
										   }
									   }}
							   >Видалити</Button>
							   <Button type={"button"} className='btn-modal'
									   handleClick={() => {
										   dispatch(actionModal(false))
									   }}>Відмінити</Button>
						   </div>
					   }
				/>)
			}
		</>
	)
}

export default CheckoutPage;