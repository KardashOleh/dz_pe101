import './NotFoundPage.scss'
import notFoundImg from '../../assets/images/not_found.png'
import Button from '../../components/Button/Button'
import {useLocation, Link } from "react-router-dom"


const NotFoundPage = () => {
	const {pathname} = useLocation()


	return (
		<div className='not-found'>
			<h2> Сторінка: <span >{pathname}</span> не знайдена </h2>
			<img src={notFoundImg} alt="not-found" />
			<Link to={'/'}>
			<Button>На головну сторінку</Button>
			</Link>
		</div>
	)
}

export default NotFoundPage;