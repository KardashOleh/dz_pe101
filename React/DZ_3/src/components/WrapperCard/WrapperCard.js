import Card from "./Card/Card";
import './WrapperCard.scss'
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import PropTypes from "prop-types";

const WrapperCard = (props) => {
	const {
		cards,
		cardsFavorite,
		setCurrentCard,
		addToFavorite,
		handleModal,
		isOpenModal,
		currentCard,
		addToCart
	} = props;

	return (
		<div>
			cards && {<ul className='list'>
			{cards.map(({url, name, author, price, sku}) => (
				<li key={sku}>
					<Card
						isFavorite={cardsFavorite?.some((el) => el.id === sku)}
						setCurrentCard={setCurrentCard}
						addToFavorite={addToFavorite}
						id={sku}
						url={url}
						name={name}
						author={author}
						price={price}
						handleModal={handleModal}
					/>
				</li>
			))}
		</ul>}

			{isOpenModal && (
				<Modal closeModal={handleModal}
					   currentCard={currentCard}
					   isCloseButton
					   headerModal={'Ви дійсно хочете додати книгу в кошик:'}
					   action={
						   <div className='wrapper-btn'>
							   <Button type={"button"} className='btn-modal'
									   handleClick={() => {
										   {
											   addToCart(currentCard);
											   handleModal()
										   }
									   }}
							   >В кошик</Button>
							   <Button type={"button"} className='btn-modal'
									   handleClick={handleModal}>Відмінити</Button>
						   </div>
					   }
				/>)
			}
		</div>
	)
}

WrapperCard.propTypes = {
	cards: PropTypes.array,
	cardsFavorite: PropTypes.array,
	currentCard: PropTypes.object,
	handleModal: PropTypes.func,
	isOpenModal: PropTypes.bool,
	setCurrentCard: PropTypes.func,
	addToFavorite: PropTypes.func,
	addToCart: PropTypes.func,
}

WrapperCard.defaultProps = {
	cardsFavorite: [],
	cards: [],
	currentCard:{},
	setCurrentCard: () => {
	},
	addToFavorite: () => {
	},
	addToCart: () => {
	},
	isOpenModal : false,
	handleModal: () =>{},
}

export default WrapperCard;



