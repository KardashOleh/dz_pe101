import './CartPage.scss'
import Button from "../../components/Button/Button";
import {ReactComponent as DelIcon} from './icons/delete.svg'
import Modal from "../../components/Modal/Modal";
import PropTypes from "prop-types";

const CartPage = ({
					  carts,
					  incrementCartItem,
					  decrementCartItem,
					  handleModal,
					  deleteCartItem,
					  isOpenModal,
					  currentCard,
					  setCurrentCard
				  }) => {

	return (

		<>
			<h1>Ваш кошик</h1>
			<ul className='list-cart'>
				{carts?.map(({url, name, count, price, id}) => (
					<li key={id}>
						<div className='wrapper-img'>
							<img src={url} alt={name}/>
							<p>{name}</p>
						</div>
						<div className='wrapper-btn-cart'>
							<span>{count}</span>
							<Button className='btn-count' handleClick={() => {
								incrementCartItem(id)
							}}>+</Button>
							<Button className='btn-count' handleClick={() => {
								decrementCartItem(id)
							}}>-</Button>
							<Button handleClick={() => {
								handleModal();
								setCurrentCard({id, name, url, price})
							}}><DelIcon/></Button>
						</div>
						<span>{price} грн</span>
						<div className='total-item'>{price * count} грн</div>
					</li>
				))}
				<span className='total-price'>Всього: {carts.map(({
																	  price,
																	  count
																  }) => price * count).reduce((prev, curr) => prev + curr, 0)} грн</span>
			</ul>

			{isOpenModal && (
				<Modal closeModal={handleModal}
					   currentCard={currentCard}
					   isCloseButton
					   headerModal={'Ви дійсно хочете видалити книгу з кошика:'}
					   action={
						   <div className='wrapper-btn'>
							   <Button type={"button"} className='btn-modal'
									   handleClick={() => {
										   {
											   deleteCartItem(currentCard)
											   handleModal()
										   }
									   }}
							   >Видалити</Button>
							   <Button type={"button"} className='btn-modal'
									   handleClick={handleModal}>Відмінити</Button>
						   </div>
					   }
				/>)
			}
		</>
	)
}

CartPage.propTypes = {
	carts: PropTypes.array,
	incrementCartItem: PropTypes.func,
	decrementCartItem: PropTypes.func,
	handleModal: PropTypes.func,
	deleteCartItem: PropTypes.func,
	isOpenModal: PropTypes.bool,
	setCurrentCard: PropTypes.func,
	currentCard: PropTypes.object,
}

CartPage.defaultProps = {
	carts: [],
	incrementCartItem: () => {
	},
	decrementCartItem: () => {
	},
	handleModal: () => {
	},
	deleteCartItem: () => {
	},
	isOpenModal: false,
	currentCard: {},
	setCurrentCard: () => {
	},
}

export default CartPage;