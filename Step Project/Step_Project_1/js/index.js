"use strict";

function tabs(btnsSelector, dataAtr, parentEl, btnsActiveClass, activeClass) {
    const buttons = document.querySelectorAll(btnsSelector);
    buttons.forEach((btn) => {
        btn.addEventListener('click', ({ target }) => {
            let buttonAtr = target.getAttribute(dataAtr);
            // console.log(parentEl, dataAtr, buttonAtr);
            let text = document.querySelector(`${parentEl} [${dataAtr} = ${buttonAtr}]`);
            document.querySelector(`.${activeClass}`).classList.remove(activeClass);
            text.classList.add(activeClass);
            buttons.forEach(element => {
                element.classList.remove(btnsActiveClass);
            });
            target.classList.add(btnsActiveClass);
        });
    });
};

tabs('.web_menu_item', 'data-text', '.rectangle', 'web_one', 'ectiv');
// tabs('.ux_photo', 'data-slider', '.slider_list', 'img_activ', 'show');


const buttonWork = document.querySelectorAll('.work_item');
buttonWork.forEach((btn) => {
    btn.addEventListener('click', clickButton);
});

function clickButton() {
    let buttonAtr = this.getAttribute('data-text');
    let imgItems = document.querySelectorAll('.item_photo');
    if (buttonAtr === 'All') {
        imgItems.forEach((el) => el.classList.remove('hiden'));
    } else {
        imgItems.forEach((el) => {
            if (el.getAttribute('data-text') === buttonAtr) {
                el.classList.remove('hiden');
            } else {
                el.classList.add('hiden');
            };
        });
    }; document.querySelector('.work_item.work_ectiv').classList.remove('work_ectiv');
    this.classList.add('work_ectiv');
};


const btnFilter = document.querySelector('#btn_filter');

const randomData = () => {
    let res = '';

    const arr = ['Graphic_Design', 'Web_Design', 'Landing_Pages', 'Wordpress'];
    let rand = Math.floor(Math.random() * arr.length);
    res = arr[rand];
    return res;
};

let filterCounter = 0;
btnFilter.addEventListener('click', () => {
    filterCounter++;
    if (filterCounter >= 2) {
        btnFilter.remove();
    }
    const perent = document.querySelector('.menu_photo');
    for (let i = 23; i <= 34; i++) {
        perent.innerHTML += `
        <li class="item_photo" data-text="${randomData()}">
        <a href="#" class="link_photo">

            <img src="./images/layer_${i}.jpg" class="img_photo" width="285px" height="211px"
                alt="photo">

            <div class="div_photo">

                <div class="div_photo_two">
                    <svg width="88" class="svg" height="43" viewBox="0 0 88 43" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <rect x="1" y="2" width="41" height="40" rx="20" stroke="#18CFAB" />
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M26.9131 17.7282L25.0948 15.8913C24.2902 15.0809 22.983 15.0759 22.1768 15.8826L20.1592 17.8926C19.3516 18.6989 19.3482 20.0103 20.1505 20.8207L21.3035 19.689C21.1868 19.3284 21.3304 18.9153 21.6159 18.6295L22.8995 17.3519C23.3061 16.9462 23.9584 16.9491 24.3595 17.3543L25.4513 18.458C25.8528 18.8628 25.8511 19.5171 25.447 19.9232L24.1634 21.2024C23.8918 21.473 23.4461 21.6217 23.1002 21.5263L21.9709 22.6589C22.7745 23.4718 24.0803 23.4747 24.8889 22.6684L26.9039 20.6592C27.7141 19.8525 27.7167 18.5398 26.9131 17.7282ZM19.5261 25.0918C19.6219 25.4441 19.4686 25.8997 19.1909 26.1777L17.9923 27.3752C17.5807 27.7845 16.916 27.7833 16.5067 27.369L15.393 26.2475C14.9847 25.8349 14.9873 25.1633 15.3982 24.7547L16.598 23.5577C16.8903 23.2661 17.3104 23.1202 17.6771 23.2438L18.8335 22.0715C18.0149 21.2462 16.6825 21.2421 15.8606 22.0632L13.9152 24.0042C13.0923 24.8266 13.0884 26.1629 13.9065 26.9886L15.7582 28.8618C16.576 29.6846 17.9072 29.6912 18.7311 28.8701L20.6765 26.9287C21.4985 26.1054 21.5024 24.7717 20.6855 23.9443L19.5261 25.0918ZM19.2579 24.5631C18.9801 24.8419 18.5343 24.8411 18.2618 24.5581C17.9879 24.2743 17.9901 23.8204 18.2661 23.5399L21.5907 20.1611C21.8668 19.8823 22.3117 19.8831 22.5851 20.164C22.8605 20.4457 22.8588 20.9009 22.5817 21.183L19.2579 24.5631Z" fill="#1FDAB5" />
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M66.5973 1.99795C77.8653 1.99795 86.9999 10.9523 86.9999 21.9979C86.9999 33.0432 77.8653 41.9979 66.5973 41.9979C55.3292 41.9979 46.1946 33.0432 46.1946 21.9979C46.1946 10.9523 55.3292 1.99795 66.5973 1.99795Z" fill="#18CFAB" />
                        <rect x="60" y="17" width="12" height="11" fill="white" />
                    </svg>

                    <p class="text_photo">CREATIVE DESIGN</p>
                    <p class="text_photo_one">Web Design</p>
                </div>

            </div>
        </a>
    </li>
        `
    }
})

const slider = document.querySelector('#slider');
const sliderItems = Array.from(slider.children);
const btnNext = document.querySelector('#right');
const btnPrev = document.querySelector('#left');
const imageCollection = document.querySelectorAll('.ux_photo');

sliderItems.forEach(function (slide, index) {
	
	if (index !== 0) slide.classList.add('hidden');
	
	slide.dataset.index = index;
	
	sliderItems[0].setAttribute('data-active', '');
});
imageCollection.forEach(function (slide, index) {
    
    slide.dataset.index = index;
    imageCollection[0].setAttribute('data-active', '');
	slide.addEventListener('click', function () {
		showNextSlide('next');
	});
});

btnNext.onclick = function () {
	showNextSlide('next');
};
btnPrev.onclick = function () {
	showNextSlide('prev');
};

function showNextSlide(direction) {
	
	const currentSlide = slider.querySelector('[data-active]');
	const currentSlideIndex = +currentSlide.dataset.index;
	currentSlide.classList.add('hidden');
	currentSlide.removeAttribute('data-active');
	
	let nextSlideIndex;
	if (direction === 'next') {
		nextSlideIndex = currentSlideIndex + 1 === sliderItems.length ? 0 : currentSlideIndex + 1;
	} else if (direction === 'prev') {
		nextSlideIndex = currentSlideIndex === 0 ? sliderItems.length - 1 : currentSlideIndex - 1;
	}
    for (const img of imageCollection) {
        img.classList.remove('img_activ');
    };
    imageCollection[nextSlideIndex].classList.add('img_activ');
	
	const nextSlide = slider.querySelector(`[data-index="${nextSlideIndex}"]`);
	nextSlide.classList.remove('hidden');
	nextSlide.setAttribute('data-active', '');
}


