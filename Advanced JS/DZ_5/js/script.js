import { createPostsArr, deletePost, putPost } from "./services.js";
import Modal from "./Modal.js";

const root = document.querySelector("#root");
const spinner = document.querySelector(".spinner");

const renderCards = (posts) => {
  const cardsList = document.createElement("ul");
  cardsList.classList.add("cards-list");

  posts.forEach((card) => {
    cardsList.append(card.render());
  });

  root.append(cardsList);

  deleteCard();
  editCard();
};

const deleteCard = () => {
  document.querySelector(".cards-list").addEventListener("click", (e) => {
    if (e.target.classList.contains("card__delete-btn")) {
      const cardForDelete = e.target.closest(".card");
      deletePost(cardForDelete.getAttribute("data-post-id")).then(console.log);
      cardForDelete.remove();
    }
  });
};

const addCard = () => {
  const addCardBtn = document.createElement("button");
  addCardBtn.classList.add("btn", "add-card-btn");
  addCardBtn.innerText = "Add post";
  root.append(addCardBtn);

  addCardBtn.style.display = "block";
  addCardBtn.addEventListener("click", () => {
    const modal = new Modal({
      headerTitle: "Add new post.",
    });

    document.body.append(modal.render());
  });
};

const editCard = () => {
  let card, cardEditBtn, cardSaveBtn, postId, userId, title, body;

  root.addEventListener("click", ({ target }) => {
    if (target.classList.contains("card__edit-btn")) {
      card = target.closest(".card");
      cardEditBtn = card.querySelector(".card__edit-btn");
      cardSaveBtn = card.querySelector(".card__save-btn");
      postId = card.getAttribute("data-post-id");
      userId = card.getAttribute("data-user-id");
      title = card.querySelector(".card__title");
      body = card.querySelector(".card__text");

      cardEditBtn.style.display = "none";
      cardSaveBtn.style.display = "inline-block";

      title.innerHTML = `
				<input
				type="text"
				value="${title.innerText}"
				/>
				`;
      body.innerHTML = `<textarea>${body.innerText}</textarea>`;
    } else if (target.classList.contains("card__save-btn")) {
      cardEditBtn.style.display = "inline-block";
      cardSaveBtn.style.display = "none";

      title.innerHTML = title.querySelector("input").value;
      body.innerHTML = body.querySelector("textarea").value;

      putPost(postId, {
        id: postId,
        body: body.innerText,
        title: title.innerText,
        userId,
      }).then(console.log);
    }
  });
};

createPostsArr().then((posts) => {
  renderCards(posts);
  spinner.style.display = "none";
  addCard();
});

