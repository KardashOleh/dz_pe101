const root = document.querySelector('#root');
const ul = document.createElement('ul');

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

books.forEach((book, i) => {
    try {
        const { author, name, price } = book;

        if (!author) {
            throw new Error(`Нет автора в ${i + 1}й книге`);
        };
        if (!name) {
            throw new Error(`Нет названия в ${i + 1}й книге`);
        };
        if (!price) {
            throw new Error(`Нет цены в ${i + 1}й книге`);
        };

        const li = document.createElement('li');
        li.innerHTML = `
        <p><b>Author: </b>${author}</p>
        <p><b>Name: </b>${name}</p>
        <p><b>Price: </b>${price}</p>
        `;
        ul.append(li);
    } catch (error) {
        console.log(error);
    };
});

root.append(ul);


