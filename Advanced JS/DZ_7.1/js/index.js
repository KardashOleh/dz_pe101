"use strict";
const themeButton = document.querySelector('.theme');
const allButtons = document.querySelectorAll('.button');
const table = document.querySelector('.my_table');
const alltable = document.querySelectorAll('.my_td');

const light = document.querySelector('.light');
const average = document.querySelector('.average');
const difficult = document.querySelector('.difficult');
const stop = document.querySelector('.stop');
const clean = document.querySelector('.clean');
const holes = [...document.querySelectorAll('.my_td')];

// console.log(holes);

// =====================================================
const local = localStorage.getItem('them');
console.log(local);
if (local === 'dark') {
    for (const btn of allButtons) {
        btn.classList.add('cssBtn');
    };
    table.classList.add('table_dark');
    for (const tb of alltable) {
        tb.classList.add('table_dark');
    };
    document.body.classList.add('dark');
};

themeButton.addEventListener('click', () => {
    for (const btn of allButtons) {
        btn.classList.toggle('cssBtn');
    };
    table.classList.toggle('table_dark')
    for (const tb of alltable) {
        tb.classList.toggle('table_dark');
    };
    document.body.classList.toggle('dark');
    const local2 = localStorage.getItem('them');
    if (local2 === 'dark') {
        localStorage.removeItem('them')
    } else {
        localStorage.setItem('them', 'dark')
    };
});

// =====================================================
const scoreEl = document.querySelector('.score span');
const scoreEl1 = document.querySelector('.score1 span');
let score = 0;
let score1 = 0;
let a = null;
let b = 0;
let c = 0;
// let hole = null

function run() {
    let arr = [];
    let counter = 0;
    holes.forEach(item => {
        if (item.classList.contains('active') || item.classList.contains('active2') || item.classList.contains('active3')) {
            arr.push(counter);
            counter++;
        } else {
            counter++;
        };
    });
    console.log(arr);
    let i = '';
    do {
        i = Math.floor(Math.random() * holes.length);
    } while (arr.indexOf(i) !== -1);
    const hole = holes[i];
    hole.classList.add('active');

    function green() {
        hole.classList.remove('active');
        hole.classList.add('active2');
        score++;
        score <= 9 ? scoreEl.textContent = '0' + score : scoreEl.textContent = score;
    }
    hole.addEventListener('click', green);

    setTimeout(() => {
        hole.removeEventListener('click', green)
    }, b)

    if (arr.length === 99) {
        setTimeout(() => {
            holes.forEach(item => {
                if (item.classList.contains('active')) {
                    item.classList.remove('active');
                    item.classList.add('active3');
                    score1++;
                    score1 <= 9 ? scoreEl1.textContent = '0' + score1 : scoreEl1.textContent = score1;
                };
            })
        }, c);
        clearInterval(a);
    };

    clean.addEventListener('click', () => {
        holes.forEach(item => {
            item.classList.remove('active');
            item.classList.remove('active2');
            item.classList.remove('active3');
            arr = [];
            scoreEl.textContent = '00';
            scoreEl1.textContent = '00';
        });
    });

};

function run2() {
    holes.forEach(item => {
        if (item.classList.contains('active')) {
            item.classList.remove('active');
            item.classList.add('active3');
            score1++;
            score1 <= 9 ? scoreEl1.textContent = '0' + score1 : scoreEl1.textContent = score1;
        };
    });
    if (score > score1) {
        scoreEl.classList.remove('text_red');
        scoreEl.classList.add('text_green');
        scoreEl1.classList.remove('text_green');
        scoreEl1.classList.add('text_red');
    };
    if (score < score1) {
        scoreEl.classList.add('text_red');
        scoreEl.classList.remove('text_green');
        scoreEl1.classList.add('text_green');
        scoreEl1.classList.remove('text_red');
    };
    if (score === score1) {
        scoreEl.classList.remove('text_red');
        scoreEl.classList.remove('text_green');
        scoreEl1.classList.remove('text_green');
        scoreEl1.classList.remove('text_red');
    };
    run();
};

light.addEventListener('click', () => {
    a = setInterval(run2, 1500);
    b = 1470;
    c = 1500;
});

average.addEventListener('click', () => {
    a = setInterval(run2, 1000);
    b = 970;
    c = 1000;
});

difficult.addEventListener('click', () => {
    a = setInterval(run2, 500);
    b = 470;
    c = 500;
});

stop.addEventListener('click', () => {
    clearInterval(a);
})










