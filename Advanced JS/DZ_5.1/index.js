
const root = document.querySelector('#root')
async function getUsers() {
    const resspons = await fetch("https://ajax.test-danit.com/api/json/users");
    if (resspons.status !== 200) {
        throw new Error("Што-то пішло не так!!!");
    }
    return await resspons.json();
}
// getUsers().then(console.log);


async function getPosts() {
    const resspons = await fetch("https://ajax.test-danit.com/api/json/posts");
    if (resspons.status !== 200) {
        throw new Error("Што-то пішло не так!!!");
    }
    return await resspons.json();
}
// getPosts().then(console.log);

Promise.all([getUsers(), getPosts()]).then(([users, posts]) => {
    const ul = document.createElement('ul');

    users.forEach(user => {
        posts.forEach(post => {
            if (user.id === post.userId) {
                const li = document.createElement('li');
                li.setAttribute('id', post.id)
                const delitButton = document.createElement('button');
                delitButton.textContent = 'Delit';
                li.innerHTML = `
                    <h2>${post.title}</h2>
                    <p>${post.body}</p>
                    <p>${user.name}</p>
                    <p>${user.email}</p>
                `
                li.append(delitButton);
                ul.append(li);
            }


        })
    });
    root.append(ul);

});

window.addEventListener('click', (e) => {
    if (e.target.textContent === 'Delit') {
        console.log(e.target.parentNode);
    }
})