const btn = document.querySelector("button");
const out = document.querySelector(".out");

const render = () => {
    out.innerHTML = `
		<h2>Result by ip :</h2>
		<div>Continent: </div>
		<div>Country: </div>
		<div>City: </div>
		<div>Region name: </div>
		<div>District: </div>
	`;
};
render()

const sendRequest = async (url) => {
    return await fetch(url).then((response) => {
        if (response.ok) {
            return response.json();
        }
        return new Error("Что-то пошло не так!");
    });
};

const getIp = () => sendRequest("https://api.ipify.org/?format=json");
const getDataIp = (ip) =>
    sendRequest(`http://ip-api.com/json/${ip}?fields=1589273`);

btn.addEventListener("click", () => {
    getIp().then(({ ip }) => {
        getDataIp(ip).then((data) => renderOut(ip, data));
    });
});

const renderOut = (ip, { continent, country, city, regionName, district }) => {
    out.innerHTML = `
		<h2>Result by ip ${ip}:</h2>
		<div>Continent: ${continent}</div>
		<div>Country: ${country}</div>
		<div>City: ${city}</div>
		<div>Region name: ${regionName}</div>
		<div>District: ${district ? district : "сори... район API вернул пустым"}</div>
	`;
};

