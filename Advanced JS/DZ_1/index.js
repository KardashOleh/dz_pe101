import { Programmer } from "./Programmer.js";

const user1 = new Programmer('Andey', 27, 25000, 'js');
const user2 = new Programmer('Sergey', 31, 40000, ['js', 'scss', 'html']);

console.log(user1);
console.log(user2);