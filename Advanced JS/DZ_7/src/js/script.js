"use strict";
export const themeButton = document.querySelector('.theme');
export const allButtons = document.querySelectorAll('.button');
export const table = document.querySelector('.my_table');
export const alltable = document.querySelectorAll('.my_td');

export const local = localStorage.getItem('them');

export const light = document.querySelector('.light');
export const average = document.querySelector('.average');
export const difficult = document.querySelector('.difficult');
export const stop = document.querySelector('.stop');
export const clean = document.querySelector('.clean');
export const holes = [...document.querySelectorAll('.my_td')];

export const scoreEl = document.querySelector('.score span');
export const scoreEl1 = document.querySelector('.score1 span');

const modal = document.querySelector('.modal');
const span = document.querySelector('.close');
const textClose = document.querySelector('.text_close');
const content = document.querySelector('.modal-content');

// =====================================================

// console.log(local);
if (local === 'dark') {
    for (const btn of allButtons) {
        btn.classList.add('cssBtn');
    };
    table.classList.add('table_dark');
    for (const tb of alltable) {
        tb.classList.add('table_dark');
    };
    document.body.classList.add('dark');
    content.classList.add('darkModal');
    // modal.classList.add('darkBr');
};

themeButton.addEventListener('click', () => {
    for (const btn of allButtons) {
        btn.classList.toggle('cssBtn');
    };
    table.classList.toggle('table_dark')
    for (const tb of alltable) {
        tb.classList.toggle('table_dark');
    };
    document.body.classList.toggle('dark');
    content.classList.toggle('darkModal');
    // modal.classList.toggle('darkBr');
    const local2 = localStorage.getItem('them');
    if (local2 === 'dark') {
        localStorage.removeItem('them')
    } else {
        localStorage.setItem('them', 'dark')
    };
});

// =====================================================
let score = 0;
let score1 = 0;
let a = null;
let b = 0;
let c = 0;
// let hole = null

function run() {
    let arr = [];
    let counter = 0;
    holes.forEach(item => {
        if (item.classList.contains('active') || item.classList.contains('active2') || item.classList.contains('active3')) {
            arr.push(counter);
            counter++;
        } else {
            counter++;
        };
    });
    // console.log(arr);
    let i = '';
    do {
        i = Math.floor(Math.random() * holes.length);
    } while (arr.indexOf(i) !== -1);
    const hole = holes[i];
    hole.classList.add('active');

    function green() {
        hole.classList.remove('active');
        hole.classList.add('active2');
        score++;
        score <= 9 ? scoreEl.textContent = '0' + score : scoreEl.textContent = score;
    }
    hole.addEventListener('mousedown', green);

    setTimeout(() => {
        hole.removeEventListener('mousedown', green)
    }, b)

    if (arr.length === 99) {
        setTimeout(() => {
            holes.forEach(item => {
                if (item.classList.contains('active')) {
                    item.classList.remove('active');
                    item.classList.add('active3');
                    score1++;
                    score1 <= 9 ? scoreEl1.textContent = '0' + score1 : scoreEl1.textContent = score1;
                };
            })
            modal.style.display = "block";
            if (score > score1) {
                textClose.textContent = `Виграли ви з рахунком: ${score}`;
                content.style.boxShadow = '2px 2px 30px #00ff15';
                // modal.style.backgroundColor = '#00ff1580';
                textClose.classList.add('text_green');
            } else {
                textClose.textContent = `Виграв коьп'ютер з рахунком: ${score1}`
                content.style.boxShadow = '2px 2px 30px #ff0000';
                // modal.style.backgroundColor = '#ff000080';
                textClose.classList.add('text_red');
            };
        }, c);
        clearInterval(a);

    };
    function closeClear() {
        score = 0;
        score1 = 0;
        holes.forEach(item => {
            item.classList.remove('active');
            item.classList.remove('active2');
            item.classList.remove('active3');
            arr = [];
            scoreEl.textContent = '00';
            scoreEl1.textContent = '00';
        });
        textClose.textContent = '';
        textClose.classList.remove('text_green');
        textClose.classList.remove('text_red');
    };

    clean.addEventListener('click', () => {
        clearInterval(a);
        closeClear()
    });
    span.addEventListener('click', () => {
        modal.style.display = "none";
        closeClear()
    });
    // window.addEventListener('click', event => {
    //     if (event.target == modal) {
    //         closeClear()
    //     }
    // });
};

function run2() {
    holes.forEach(item => {
        if (item.classList.contains('active')) {
            item.classList.remove('active');
            item.classList.add('active3');
            score1++;
            score1 <= 9 ? scoreEl1.textContent = '0' + score1 : scoreEl1.textContent = score1;
        };
    });
    if (score > score1) {
        scoreEl.classList.remove('text_red');
        scoreEl.classList.add('text_green');
        scoreEl1.classList.remove('text_green');
        scoreEl1.classList.add('text_red');
    };
    if (score < score1) {
        scoreEl.classList.add('text_red');
        scoreEl.classList.remove('text_green');
        scoreEl1.classList.add('text_green');
        scoreEl1.classList.remove('text_red');
    };
    if (score === score1) {
        scoreEl.classList.remove('text_red');
        scoreEl.classList.remove('text_green');
        scoreEl1.classList.remove('text_green');
        scoreEl1.classList.remove('text_red');
    };
    run();
};

light.addEventListener('click', () => {
    a = setInterval(run2, 1500);
    b = 1470;
    c = 1500;
});

average.addEventListener('click', () => {
    a = setInterval(run2, 1000);
    b = 970;
    c = 1000;
});

difficult.addEventListener('click', () => {
    a = setInterval(run2, 500);
    b = 470;
    c = 500;
});

stop.addEventListener('click', () => {
    clearInterval(a);
});


