// const navBar = document.querySelector(".nav");
// const links = document.querySelectorAll(".link");
// const hamMenu = document.querySelector(".ham-menu");
// const hamMenuContent = document.querySelector(".ham-menu-content");
// let device = window.innerWidth > 769 ? "desktop" : "mobile";

// const handleShowHamMenu = () => {
//   links.forEach(
//     (item) => (item.style.display = device === "mobile" ? "none" : "block")
//   );
//   device === "mobile"
//     ? hamMenu.classList.add("show")
//     : hamMenu.classList.remove("show");
// };

// handleShowHamMenu();

// window.addEventListener("resize", (e) => {
//   // console.log(e);
//   device = window.innerWidth > 769 ? "desktop" : "mobile";
//   handleShowHamMenu();
// });

// hamMenu.addEventListener("click", () => {
//   hamMenu.classList.toggle("active");
//   if (!hamMenuContent.classList.contains("show")) {
//     hamMenuContent.classList.remove("hide");
//     hamMenuContent.classList.add("show");
//   } else {
//     hamMenuContent.classList.remove("show");
//     hamMenuContent.classList.add("hide");
//   }
// });



const header = () => {
  const body = document.body;
  const menu = document.querySelector('.menu');
  const menuBtn = document.querySelector('.header__menu-btn');
  const menuBtnTopLine = document.querySelector('.header__menu-btn-top');
  const menuBtnMiddleLine = document.querySelector('.header__menu-btn-middle');
  const menuBtnBottomLine = document.querySelector('.header__menu-btn-bottom');


  const menuBtnOnClick = (e) => {
    e.stopPropagation();
    menuBtnTopLine.classList.toggle('header__menu-btn-top--close');
    menuBtnMiddleLine.classList.toggle('header__menu-btn-middle--close');
    menuBtnBottomLine.classList.toggle('header__menu-btn-bottom--close');

    menu.classList.toggle('menu--shown');

    body.classList.toggle('nav-is-open');
  };

  const bodyOnClick = () => {
    if (body.classList.contains('nav-is-open')) {
      menuBtnTopLine.classList.remove('header__menu-btn-top--close');
      menuBtnMiddleLine.classList.remove('header__menu-btn-middle--close');
      menuBtnBottomLine.classList.remove('header__menu-btn-bottom--close');

      menu.classList.remove('menu--shown');

      body.classList.remove('nav-is-open');
    }
  };

  document.body.addEventListener('click', bodyOnClick);
  menuBtn.addEventListener('click', menuBtnOnClick);
};

header();
