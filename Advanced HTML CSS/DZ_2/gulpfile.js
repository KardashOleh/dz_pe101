import gulp from "gulp";
const { src, dest, watch, series, parallel } = gulp;
import autoprefixer from "gulp-autoprefixer";
import cleanCSS from "gulp-clean-css";

import browserSync from "browser-sync";
const bsServer = browserSync.create();

import dartSass from "sass";
import gulpSass from "gulp-sass";
const sass = gulpSass(dartSass);

function serve() {
    bsServer.init({
        server: {
            baseDir: "./",
            browser: "chrome",
        },
    });
}

function styles() {
    return src("./src/styles/style.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(
            autoprefixer(["last 15 versions", "> 1%", "ie 8", "ie 7"], {
                cascade: true,
            })
        )
        // .pipe(cleanCSS({ compatibility: "ie8" }))
        .pipe(dest("./dist/css/"))
        .pipe(bsServer.reload({ stream: true }));
}

function scripts() {
    return src("./src/js/**/*.js")
        .pipe(dest("./dist/js"))
        .pipe(bsServer.reload({ stream: true }))
}

function images() {
    return src("./src/img/**/*.{jpg,jpeg,png,svg,webp}")
        .pipe(dest("./dist/img"))
        .pipe(bsServer.reload({ stream: true }))
}

function watcher() {
    watch("./src/styles/**/*.scss", styles);

    watch("*.html").on("change", bsServer.reload);
    watch("./src/js/*.js").on("change", series(scripts, bsServer.reload));
    watch("./src/img/**/*.{jpg,jpeg,png,svg,webp}").on("change", series(images, bsServer.reload));
    watch("./src/img/**/*.{jpg,jpeg,png,svg,webp}").on(
        "change",
        series(images, bsServer.reload)
    );
}

export const dev = series(styles, scripts, images, parallel(serve, watcher));
